@extends('website-cms::base')

@section('content')
    <div class="meta">
        {{csrf_field()}}
        <input type="hidden" name="model" value="shop_orders">

    </div>
    <div class="containerList">
        <div class="row">
            <div class="col-md-12">
                <br/>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="https://cms.gula.nl/resizer/36x36/cms/icons/shop.png"/> Bestellingen
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table table-borderless table-sm table-hover" id="listData">
                            <thead>
                            <tr>
                                <td align="right" width="100px;">OrderNr:</td>
                                <td align="right" width="100px;">Bedrag:</td>
                                <td width="100px;">Betaald:</td>
                                <td>Levermoment:</td>
                                <td>Klant:</td>
                                <td>E-mail:</td>
                                <td>Adres:</td>
                                <td>Opmerking:</td>
                                <td>Status:</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr style="color: {{$order->payment_status === '1' ? 'black' : 'lightgrey'}}">
                                    <td align="right">{{$order->id_order}}</td>
                                    <td align="right">{{number_format($order->amount, 2)}} </td>
                                    <td>{{ ($order->payment_status === '1' ? 'Ja' : 'Nee') }}</td>
                                    <td>{{$order->delivery_day . ' om ' . $order->delivery_time . ' uur'}}</td>
                                    <td>{{$order->surname . (!empty($order->pre_lastname) ? ' ' . $order->pre_lastname : '') . ' ' . $order->lastname}}</td>
                                    <td>{{$order->email}}</td>
                                    <td>{{$order->city . ', ' . $order->street . ' ' . $order->house_number . (!empty($order->house_number_addition) ? ' ' . $order->house_number_addition : '')}}</td>
                                    <td>{{$order->remarks}}</td>
                                    <td>{{$order->process_status}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

