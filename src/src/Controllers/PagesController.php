<?php


namespace Gula\WebsiteCms\Controllers;


use Gula\WebsiteCms\Models\Pages;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PagesController extends Controller
{
    protected $table = 'pages';

    public function edit(int $id)
    {
        $mdl = new Pages();
        $record = $mdl->getOne($id);

        $scope = [
            'table' => $mdl->getTableName(),
            'title' => 'Pagina aanpassen',
            'icon' => 'https://cms.gula.nl/resizer/36x36/cms/icons/titles.png',
        ];

        $viewFile = 'website-cms::edit_' . $mdl->getTableName();

        return view($viewFile, compact('record', 'scope'));
    }

    public function store(Request $request)
    {
        $post = $request->all();

        $mdl = new Pages();
        $mdl->store($post);

        return redirect('/cms');
    }

}
