<?php


namespace Gula\WebsiteCms\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Gula\Fileupload\Controllers\FileuploadController;
use Gula\ListFilter\ListFilterController;
use Gula\WebsiteCms\Models\ShopCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WebsiteCmsController extends Controller
{
    public function __construct()
    {
        //auth regelen
//        die('websiteCmsController');
    }

    public function start()
    {
        return view('website-cms::home');
        die('functie start');
    }

    public function store(Request $request, $table)
    {
        $post = $request->all();
        unset($post['_token']);

        DB::table($table)->where('id', '=', $post['id'])->update($post);

        return redirect('/cms/' . $table . '/list');
    }

    public function imageManager(string $folder = null, string $subfolder = null)
    {
        $folder .= ($subfolder !== null ? '/' . $subfolder : '');
        $object = new FileuploadController();

        return $object->getFolder($folder);
    }

    public function add(string $table)
    {
        $id = DB::table($table)->insertGetId(['deleted' => 0]);

        return redirect('/cms/' . $table . '/edit/' . $id);
    }
}


