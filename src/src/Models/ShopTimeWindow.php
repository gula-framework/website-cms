<?php


namespace Gula\WebsiteCms\Models;


use Illuminate\Database\Eloquent\Model;

class ShopTimeWindow extends Model
{
    protected $table = 'shop_taxes';
    protected $guarded = [];
    protected $deadlineDay = 'Wednesday';

    protected $dateTranslations = [
        'Monday' => 'maandag',
        'Tuesday' => 'dinsdag',
        'Wednesday' => 'woensdag',
        'Thursday' => 'donderdag',
        'Friday' => 'vrijdag',
        'Saturday' => 'zaterdag',
        'Sunday' => 'zondag',
        'January' => 'januari',
        'February' => 'februari',
        'March' => 'mei',
        'April' => 'april',
        'May' => 'mei',
        'June' => 'juni',
        'July' => 'juli',
        'August' => 'augustus',
        'September' => 'september',
        'October' => 'oktober',
        'November' => 'november',
        'December' => 'december',
    ];

    public function getTableName()
    {
        return $this->table;
    }

    private function getDeliveryMoments()
    {
        return [
            'days' => [
                '5' => 'Friday',
                '6' => 'Saturday',
            ],
            'time' => [
                'from' => '16:00',
                'till' => '20:00'
            ],
        ];
    }

    public function getDeliveryInfo(int $weeks = 4): array
    {
        $nextDeadline = date('Y-n-d', strtotime('next ' . $this->deadlineDay));
        $deliveryMoments = $this->getDeliveryMoments();
        $deliveryInfo = [];

        foreach ($deliveryMoments['days'] as $key => $day) {
            $date = new \DateTime($nextDeadline);

            for ($i = 0; $i < $weeks; $i++) {
                $date->modify('next ' . $day);
                $deliveryDates[] = $date->format('Y-m-d');
            }
        }
        asort($deliveryDates);

        foreach ($deliveryDates as &$dDate) {
            $dDate = $this->formatDate('l j F Y', $dDate);

        }

        $deliveryInfo = [
            'dates' => $deliveryDates,
        ];

        $time = date('H:i', strtotime($deliveryMoments['time']['from']));
        $time = new \DateTime($time);
        $deliveryTimes[] = $time->format('H:i');

        do {
            $time->modify('+15 minutes');
            $deliveryTimes[] = $time->format('H:i');
        } while ($time->format('H:i') < $deliveryMoments['time']['till']);

        $deliveryInfo['times'] = $deliveryTimes;

        return $deliveryInfo;
    }

    private function formatDate($format, $date)
    {
        $dateString = date('l j F Y', strtotime($date));

        foreach ($this->dateTranslations as $origin => $translation) {
            $dateString = str_replace($origin, $translation, $dateString);
        }

        return $dateString;
    }

}
