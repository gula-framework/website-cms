<?php


namespace Gula\WebsiteCms\Models;


use Illuminate\Support\Facades\DB;

class ShopTemplates
{
    protected $table = 'shop_templates';
    protected $guarded = [];

    public function getOne(string $name_constant){
        return DB::table($this->table)->where(['name_constant' => $name_constant])->first();
    }

    public function getTableName(){
        return $this->table;
    }

    public function getTemplates()
    {
        return DB::table($this->table)->where(['deleted' => false])->get();
    }

}
