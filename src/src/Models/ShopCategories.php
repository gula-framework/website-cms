<?php

namespace Gula\WebsiteCms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShopCategories
{
    protected $table = 'shop_categories';
    protected $guarded = [];

    public function getOne(int $id){
        return DB::table($this->table)->where(['id' => $id])->first();
    }

    public function getTableName(){
        return $this->table;
    }

    public function getCategories()
    {
        return DB::table($this->table)->where(['deleted' => false])->get();
    }
}
