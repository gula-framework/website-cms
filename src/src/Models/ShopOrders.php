<?php


namespace Gula\WebsiteCms\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShopOrders extends Model
{
    protected $table = 'shop_orders';
    protected $tableUsers = 'users';

    public function getOne(int $id){
        return DB::table($this->table)->where(['id' => $id])->first();
    }

    public function getTableName(){
        return $this->table;
    }

    /**
     * @param array $customer
     * @return int
     */
    public function updateUser(array $customer):int
    {
        unset($customer['remarks']);
        unset($customer['delivery_day']);
        unset($customer['delivery_time']);

        $user = DB::table($this->tableUsers)->where(['email' => $customer['email']])->first();

        if(!$user){
            $idUser = DB::table($this->tableUsers)->insertGetId(['email' => $customer['email']]);
        } else {
            $idUser = $user->id;
        }

        DB::table($this->tableUsers)
            ->where(['id' => $idUser])
            ->update($customer);

        return $idUser;
    }

    /**
     * @param array $orderData
     * @return int
     */
    public function updateOrder(array $orderData):int
    {
        $order = DB::table($this->table)->where(['id_session' => $orderData['id_session'], 'id_user' => $orderData['id_user'], 'deleted' => 0])->first();

        if(!$order){
            $idOrder= DB::table($this->table)->insertGetId($orderData);
        } else {
            $idOrder = $order->id;
        }

        DB::table($this->table)
            ->where(['id' => $idOrder])
            ->update($orderData);

        return $idOrder;
    }


}
