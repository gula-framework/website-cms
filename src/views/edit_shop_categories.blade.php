@extends('website-cms::base')

@section('content')
    <div class="meta">
        {{csrf_field()}}
        <input type="hidden" name="model" value="{{$scope['table']}}">

    </div>
    <div class="containerList">
        <div class="row">
            <div class="col-md-12">
                <br/>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ $scope['icon'] }}"/> {{ $scope['title'] }}
                            </div>
                            <div class="col-md-10">
                                Wijzigen / Toevoegen
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="post" action="/cms/{{$scope['table']}}/store" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$record->id}}">

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general"
                                       role="tab" aria-controls="general" aria-selected="true">Algemeen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab"
                                       aria-controls="seo" aria-selected="false">SEO</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab"
                                       aria-controls="images" aria-selected="false">Afbeelding</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="general" role="tabpanel"
                                     aria-labelledby="general-tab">
                                    <div class="row">
                                        <div class="col-6">
                                            <br/>
                                            <div class="form-group col-8">
                                                <label for="name">Titel:</label>
                                                <input type="text" class="form-control" name="name"
                                                       value="{{$record->name}}">
                                            </div>
                                            <div class="form-group col-3">
                                                <label for="name">Bovenliggende categorie:</label>
                                                <select name="id_parent_category" class="form-control">
                                                    <option value="">Geen</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}"
                                                                @if($record->id_parent_category === $category->id) selected @endif>{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-10">
                                                <label for="description">Beschrijving</label>
                                                <textarea name="description"
                                                          class="form-control">{{$record->description}}</textarea>
                                            </div>
                                            <div class="form-group col-2">
                                                <label for="name">Verwijderd:</label>
                                                <select name="deleted" class="form-control">
                                                    <option value=0 @if($record->deleted == false) selected @endif>Nee
                                                    </option>
                                                    <option value=1 @if($record->deleted == true) selected @endif>Ja
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                                    <div class="row">
                                        <div class="col-6">
                                            <br/>
                                            <div class="form-group col-3">
                                                <label for="slug">Slug:</label>
                                                <input type="text" name="slug" class="form-control"
                                                       value="{{$record->slug}}" readonly="readonly">
                                            </div>
                                            <div class="form-group col-4">
                                                <label for="meta_title">Meta Title:</label>
                                                <input type="text" class="form-control" name="meta_title" maxlength="50"
                                                       value="{{$record->meta_title}}">
                                            </div>
                                            <div class="form-group col-12">
                                                <label for="meta_description">Meta Description</label>
                                                <textarea name="meta_description" maxlength="200"
                                                          class="form-control">{{$record->meta_description}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                                    <br/>
                                    <div class="row">
                                        <div class="col-3">
                                            <input type="hidden" id="image" name="image" value="{{$record->image}}" />
                                            @if($record->image)
                                                <img id="category_image" src="https://cms.gula.nl/resizer/300x300/{{env('APP_CDN')}}/categorie/{{$record->image}}">
                                            @else
                                                <div id="image" class="">
                                                    <img id="category_image" src="https://cms.gula.nl/resizer/300x300/cms/general/empty-image.jpg">
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-4">
                                            <button class="btn btn-info" type="button" onclick="selectImage('category');false">Afbeelding kiezen</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group col-3">
                                        <button type="submit" class="btn btn-success">Opslaan</button>
                                    </div>
                                </div>
                                <div class="col-6"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="selectImages" tabindex="-1" role="dialog" aria-labelledby="selectImages" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Kies een afbeelding</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="imageId" value="0" />
                        @foreach($scope['images'] as $image)
                        <div class="col-2" onclick="setImage('{{$image}}');">
                            <img src="{{env('PATH_CDN')}}150x150/{{env('APP_CDN')}}/categorie/{{$image}}" class="thumb" />
                            <small>{{$image}}</small>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('js/select-images.js')}}" rel="stylesheet"></script>
@endsection
