@extends('website-cms::base')

@section('content')
    <div class="meta">
        {{csrf_field()}}
        <input type="hidden" name="model" value="{{$scope['table']}}">

    </div>
    <div class="containerList">
        <div class="row">
            <div class="col-md-12">
                <br/>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ $scope['icon'] }}"/> {{ $scope['title'] }}
                            </div>
                            <div class="col-md-10">
                                Wijzigen / Toevoegen
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="post" action="/cms/{{$scope['table']}}/store" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$record->id}}">

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general"
                                       role="tab" aria-controls="general" aria-selected="true">Algemeen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab"
                                       aria-controls="seo" aria-selected="false">SEO</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab"
                                       aria-controls="images" aria-selected="false">Afbeeldingen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="prices-tab" data-toggle="tab" href="#prices" role="tab"
                                       aria-controls="prices" aria-selected="false">Prijzen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="additional_products-tab" data-toggle="tab"
                                       href="#additional_products" role="tab"
                                       aria-controls="additional_products" aria-selected="false">Bijverkoop
                                        producten</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="general" role="tabpanel"
                                     aria-labelledby="general-tab">
                                    <div class="row">
                                        <div class="col-6">
                                            <br/>
                                            <div class="form-group col-8">
                                                <label for="name">Titel:</label>
                                                <input type="text" class="form-control" name="name"
                                                       value="{{$record->name}}">
                                            </div>
                                            <div class="form-group col-3">
                                                <label for="name">Categorie:</label>
                                                <select name="id_category" class="form-control">
                                                    <option value="">Geen</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}"
                                                                @if($record->id_category === $category->id) selected @endif>{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-10">
                                                <label for="description">Beschrijving</label>
                                                <textarea name="description"
                                                          class="form-control">{{$record->description}}</textarea>
                                            </div>
                                            <div class="form-group col-10">
                                                <label for="description_short">Beschrijving kort (tbv
                                                    overzichtpagina)</label>
                                                <textarea name="description_short"
                                                          class="form-control">{{$record->description_short}}</textarea>
                                            </div>
                                            <div class="form-group col-2">
                                                <label for="name">Verwijderd:</label>
                                                <select name="deleted" class="form-control">
                                                    <option value=0 @if($record->deleted == false) selected @endif>Nee
                                                    </option>
                                                    <option value=1 @if($record->deleted == true) selected @endif>Ja
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                                    <div class="row">
                                        <div class="col-6">
                                            <br/>
                                            <div class="form-group col-3">
                                                <label for="slug">Slug:</label>
                                                <input type="text" name="slug" class="form-control"
                                                       value="{{$record->slug}}" readonly="readonly">
                                            </div>
                                            <div class="form-group col-4">
                                                <label for="meta_title">Meta Title:</label>
                                                <input type="text" class="form-control" name="meta_title"
                                                       value="{{$record->meta_title}}">
                                            </div>
                                            <div class="form-group col-12">
                                                <label for="meta_description">Meta Description</label>
                                                <textarea name="meta_description" maxlength="200"
                                                          class="form-control">{{$record->meta_description}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                                    <br/>
                                    <div class="row">
                                        <div class="col-4">
                                            <button class="btn btn-info" type="button"
                                                    onclick="selectImage('product');false">Afbeelding toevoegen
                                            </button>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row" id="productImages">
                                        @if(count($scope['product_images']) > 0)
                                            @php
                                                $c=0;
                                            @endphp
                                            @foreach($scope['product_images'] as $productImage)
                                                @php
                                                    $c++;
                                                @endphp
                                                <input type=hidden name="image_{{$c}}"
                                                       value="{{$productImage->url}}"
                                                       data-table-id="{{$productImage->id}}"/>
                                                <div class="col-md-2 selectedImage">

                                                    <img id="product_image"
                                                         src="https://cms.gula.nl/resizer/300x300/{{env('APP_CDN')}}/product/{{$productImage->url}}">
                                                    <div class="disconnectImage">
                                                        <a href="/cms/unlink_image/{{$productImage->id}}/{{$record->id}}"
                                                           onclick="return confirmUnlink();"><img
                                                                src="https://cms.gula.nl/resizer/36x36/cms/icons/disconnect.png"
                                                                title="Afbeelding loskoppelen"/></a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            Er zijn nog geen afbeeldingen gekoppeld!
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="prices" role="tabpanel" aria-labelledby="prices-tab">
                                    <br/>
                                    <div class="row">
                                        <div class="col-6">
                                            <br/>
                                            <div class="form-group col-2">
                                                <label for="name">Btw percentage:</label>
                                                <select name="id_tax" class="form-control">
                                                    @foreach($scope['taxes'] as $tax)
                                                        <option value="{{$tax->id}}"
                                                                @if($record->id_tax == "{{$tax->id}}") selected @endif>{{$tax->name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <br/>
                                            {{--                                            Aantal personen 2 en 3 zijn verplicht. Als er geen prijs is ingegeven bij de andere aantallen, dan rekent het systeem zelf de prijs uit. De meerprijs is dan het verschil tussen de laatst ingegeven bedragen.--}}
                                            <br/>
                                            <br/>
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Aantal personen</th>
                                                    <th>Prijs</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($scope['prices'] as $key => $price)
                                                    <tr>
                                                        <td>{{$key}}</td>
                                                        <td><input type="number" name="price_{{$key}}" step="0.05"
                                                                   value="{{$price}}" min="0" class="form-control">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="additional_products" role="tabpanel"
                                     aria-labelledby="additional_products-tab">
                                    <br/>
                                    <div class="row">
                                        <div class="col-6">
                                            <br/>
                                            @foreach($scope['additional_products'] as $item)
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" name="additional_products[{{$item->id}}]"
                                                               class="form-check-input" value="{{$item->id}}"
                                                               @if(in_array($item->id, $scope['linked_additional_products'])) checked @endif>{{$item->name}}
                                                        <br/>
                                                    </label>
                                                </div>
                                                {{--@todo add checked--}}
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group col-3">
                                        <button type="submit" class="btn btn-success">Opslaan</button>
                                    </div>
                                </div>
                                <div class="col-6"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="selectImages" tabindex="-1" role="dialog" aria-labelledby="selectImages"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Kies een afbeelding</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="imageId" value="{{count($scope['product_images'])}}"/>
                        @foreach($scope['images'] as $image)
                            <div class="col-2" onclick="setMultipleImage('{{$image}}');">
                                <img src="{{env('PATH_CDN')}}150x150/{{env('APP_CDN')}}/product/{{$image}}"
                                     class="thumb"/>
                                <small>{{$image}}</small>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        var path_cdn = '{{env('PATH_CDN')}}';
        var app_cdn = '{{env('APP_CDN')}}';

        function confirmUnlink()
        {
            if (window.confirm('Wil je deze foto verwijderen van dit product?')){
                return true;
            }
            return false;
        }
    </script>
    <script src="{{asset('js/select-images.js')}}"></script>
@endsection
