<?php


namespace Gula\WebsiteCms\Controllers;


use App\Http\Controllers\Controller;
use Gula\ListFilter\ListFilterController;

class ShopCustomersController extends Controller
{
    protected $table = 'users';
    protected $tableRoles = 'roles';
    protected $tableRoleUser = 'role_user';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:ROLE_ADMIN');
    }

    public function index()
    {
        //filterbare lijst met klanten
    }

    public function list()
    {
        $object = new ListFilterController();

        $params = [
            'tableName' => $this->table,
            'select' => ['id', 'surname', 'pre_lastname', 'lastname', 'email', 'city', 'phone'],
            'join' => [],
            'filteredColumns' => ['lastname', 'email', 'city'],
            'listOrder' => ['column' => 'lastname', 'order' => 'asc'],
        ];

        return $object->getList($params);
    }


}
