<?php


namespace Gula\WebsiteCms\Controllers;

use App\Http\Controllers\Controller;
use Gula\Fileupload\Controllers\FileuploadController;
use Gula\ListFilter\ListFilterController;
use Gula\WebsiteCms\Models\ShopCategories;

class ShopCategoriesController extends Controller
{
    protected $table = 'shop_categories';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:ROLE_ADMIN');
    }

    public function list()
    {
        $object = new ListFilterController();

        $params = [
            'tableName' => $this->table,
            'select' => ['id', 'name', 'id_parent_category'],
            'join' => [],
            'filteredColumns' => ['name', 'id_parent_category'],
            'listOrder' => ['column' => 'name', 'order' => 'asc'],
            //@todo multiple listOrders?
        ];

        return $object->getList($params);
    }

    public function edit(int $id)
    {
        $mdl = new ShopCategories();
        $record = $mdl->getOne($id);
        $categories = $mdl->getCategories();
        $mdlImages = new FileuploadController();

        if(!$record->slug){
            $record->slug = str_slug($record->name, '-');
        }

        $scope = [
            'table' => $mdl->getTableName(),
            'title' => 'Categorieën',
            'icon' => 'https://cms.gula.nl/resizer/36x36/cms/icons/categories.png',
            'images' => $mdlImages->getImageList(env('PATH_IMAGE_UPLOAD') . '/categorie'),
        ];

        $viewFile = 'website-cms::edit_' . $mdl->getTableName();

        return view($viewFile, compact('record', 'scope', 'categories'));
    }

}
