<?php


namespace Gula\WebsiteCms\Controllers;


use Gula\WebsiteCms\Models\ShopAdditionalProducts;
use Illuminate\Routing\Controller;
use Gula\ListFilter\ListFilterController;

class ShopAdditionalProductsController extends Controller
{
    protected $table = 'shop_additional_products';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:ROLE_ADMIN');
    }

    public function list()
    {
        $object = new ListFilterController();

        $params = [
            'tableName' => $this->table,
            'select' => ['id', 'name', 'price'],
            'join' => [],
            'filteredColumns' => ['name'],
            'listOrder' => ['column' => 'name', 'order' => 'asc'],
            //@todo multiple listOrders?
        ];

        return $object->getList($params);
    }

    public function edit(int $id)
    {
        $mdl = new ShopAdditionalProducts();
        $record = $mdl->getOne($id);

        $scope = [
            'table' => $mdl->getTableName(),
            'title' => 'Bijverkoop',
            'icon' => 'https://cms.gula.nl/resizer/36x36/cms/icons/titles.png',
        ];

        $viewFile = 'website-cms::edit_' . $mdl->getTableName();

        return view($viewFile, compact('record', 'scope'));
    }

}
