<?php


namespace Gula\WebsiteCms\Controllers;


use App\Http\Controllers\Controller;

class ShopUserController extends Controller
{
    protected $table = 'users';
    protected $tableRoles = 'roles';
    protected $tableRoleUser = 'role_user';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:ROLE_SUPERADMIN');
    }


}
