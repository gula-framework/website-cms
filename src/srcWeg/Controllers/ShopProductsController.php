<?php


namespace Gula\WebsiteCms\Controllers;

use Gula\Fileupload\Controllers\FileuploadController;
use Gula\ListFilter\ListFilterController;
use Gula\WebsiteCms\Models\ShopAdditionalProducts;
use Gula\WebsiteCms\Models\ShopCategories;
use Gula\WebsiteCms\Models\ShopImages;
use Gula\WebsiteCms\Models\ShopPriceMatrixes;
use Gula\WebsiteCms\Models\ShopProducts;
use Gula\WebsiteCms\Models\ShopTaxes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShopProductsController
{
    protected $table = 'shop_products';
    protected $tableImages = 'shop_images';
    protected $tablePrices = 'shop_pricematrixes';
    protected $tableLinkedAdditionalProducts = 'shop_products_shop_additional_products';

    public function list()
    {
        $object = new ListFilterController();

        $params = [
            'tableName' => $this->table,
            'select' => ['id', 'name', 'id_category'],
            'join' => [],
            'filteredColumns' => ['name', 'id_category'],
            'listOrder' => ['column' => 'name', 'order' => 'asc'],
            //@todo multiple listOrders?
        ];

        return $object->getList($params);
    }

    public function edit(int $id)
    {
        $mdl = new ShopProducts();
        $record = $mdl->getOne($id);
        $mdlCategories = new ShopCategories();
        $mdlProductImages = new ShopImages();
        $mdlImages = new FileuploadController();
        $mdlPriceMatrix = new ShopPriceMatrixes();
        $mdlTaxes = new ShopTaxes();
        $mdlAdditionalProducts = new ShopAdditionalProducts();

        $categories = $mdlCategories->getCategories();

        if(!$record->slug){
            $record->slug = str_slug($record->name, '-');
        }

        $scope = [
            'table' => $mdl->getTableName(),
            'title' => 'Producten',
            'icon' => 'https://cms.gula.nl/resizer/36x36/cms/icons/products.png',
            'product_images' => $mdlProductImages->getImageSet($id),
            'images' => $mdlImages->getImageList(env('PATH_IMAGE_UPLOAD') . '/product'),
            'prices' => $mdlPriceMatrix->getPriceSet($id),
            'taxes' => $mdlTaxes->getTaxes(),
            'additional_products' => $mdlAdditionalProducts->getAdditionalProducts(),
            'linked_additional_products' => $mdlAdditionalProducts->getLinkedProducts($id),
        ];

        $viewFile = 'website-cms::edit_' . $mdl->getTableName();

        return view($viewFile, compact('record', 'scope', 'categories'));
    }
    public function store(Request $request)
    {
        $post = $request->all();
        unset($post['_token']);

        $images = [];
        foreach ($post as $key => $value){
            if(substr($key,0,6) === 'image_'){
                $images[substr($key,6)] = $value;
                unset($post[$key]);
            }
        }

        $prices = [];
        foreach ($post as $key => $value){
            if(substr($key,0,6) === 'price_'){
                $prices[substr($key,6)] = $value;
                unset($post[$key]);
            }
        }

        $additionalProducts = (key_exists('additional_products', $post) ? $post['additional_products'] : array());
        unset($post['additional_products']);

        DB::table($this->table)->where('id', '=', $post['id'])->update($post);

        DB::table($this->tableImages)
            ->where('id_item', '=', $post['id'])
            ->where('table', '=', $this->table)
            ->delete();

        foreach ($images as $key => $image){
            DB::table($this->tableImages)->insert(
                [
                    'id_item' => $post['id'],
                    'url' => $image,
                    'sort_order' => $key,
                    'table' => $this->table
                    ]
            );
        }

        DB::table($this->tablePrices)
            ->where('id_product', '=', $post['id'])
            ->delete();

        foreach ($prices as $key => $price){
            DB::table($this->tablePrices)->insert(
                [
                    'id_product' => $post['id'],
                    'amount' => $key,
                    'price' => $price,
                    ]
            );
        }

        DB::table($this->tableLinkedAdditionalProducts)
            ->where('id_product', '=', $post['id'])
            ->delete();

        foreach ($additionalProducts as $item){
            DB::table($this->tableLinkedAdditionalProducts)->insert(
                [
                    'id_product' => $post['id'],
                    'id_additional_product' => $item,
                    ]
            );
        }

        return redirect('/cms/' . $this->table . '/list');
    }


}
