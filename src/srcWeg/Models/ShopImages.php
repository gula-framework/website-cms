<?php

namespace Gula\WebsiteCms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShopImages extends Model
{
    protected $table = 'shop_images';
    protected $guarded = [];

    public function getTableName(){
        return $this->table;
    }

    public function getImageSet(int $idProduct)
    {
        return DB::table($this->table)
            ->where('id_item', '=', $idProduct)
            ->where('deleted', '=', false)
            ->orderBy('sort_order', 'desc')
            ->get();
    }
}
