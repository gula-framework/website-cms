<?php


namespace Gula\WebsiteCms\Models;


use Gula\WebsiteCms\Models\ShopPriceMatrixes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ShopCart extends Model
{
    protected $table = 'shop_cart';
    protected $tableProducts = 'shop_products';
    protected $tablePriceMatrixes = 'shop_pricematrixes';

    public function getTableName()
    {
        return $this->table;
    }

    public function addToCart(array $cartRow)
    {
        DB::table($this->table)->insert($cartRow);
    }

    public function getCart(string $sessionId)
    {
        $results = DB::table($this->table)
            ->select('shop_cart.*', 'shop_products.name')
            ->leftJoin($this->tableProducts, $this->table . '.id_product', '=', $this->tableProducts . '.id')
            ->where($this->table . '.deleted', '=', false)
            ->where('id_session', '=', $sessionId)
            ->get();

        $mdlPriceMatrix = new ShopPriceMatrixes();

        foreach ($results as &$result){
            $result->priceMatrix = $mdlPriceMatrix->getPriceSet($result->id_product);

            foreach ($result->priceMatrix as $pkey => $price){
                if($pkey === $result->amount){
                    $result->price = $price;
                }
            }
        }
        return $results;
    }

    public function removeFromCart(int $cartId, string $sessionId)
    {
        DB::table($this->table)
            ->where(['id' => $cartId, 'id_session' => $sessionId])
            ->update(['deleted' => true]);
    }

    public function changeCartItem(int $cartId, string $sessionId, int $newAmount)
    {
        DB::table($this->table)
            ->where(['id' => $cartId, 'id_session' => $sessionId])
            ->update(['amount' => $newAmount]);
    }

    public function getCartTotal(string $sessionId)
    {
        return DB::table($this->table)
            ->select(DB::raw("SUM(" . $this->tablePriceMatrixes . ".price) as total, count(*) as rows"))
            ->leftJoin($this->tablePriceMatrixes, function ($join){
                $join->on($this->tablePriceMatrixes . '.id_product', '=', $this->table . '.id_product')
                    ->on($this->tablePriceMatrixes . '.amount', '=', $this->table . '.amount');
            })
            ->where($this->table . '.deleted', '=', false)
            ->where('id_session', '=', $sessionId)
            ->get();
    }
}
