<?php


namespace Gula\WebsiteCms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShopProducts
{
    protected $table = 'shop_products';
    protected $tableImages = 'shop_images';
    protected $guarded = [];

    public function getOneBySlug(string $slug){
        return DB::table($this->table)->where(['slug' => $slug])->first();
    }

    public function getOne(int $id){
        return DB::table($this->table)->where(['id' => $id])->first();
    }

    public function getTableName(){
        return $this->table;
    }

    public function getSummaryProducts()
    {
        return DB::table($this->table)
            ->select([
                $this->table . '.*',
                DB::raw('(SELECT url FROM shop_images WHERE id_item = '. $this->table . '.id ORDER BY shop_images.url ASC LIMIT 1) as image '),
                DB::raw('(SELECT price FROM shop_pricematrixes WHERE id_product = '. $this->table . '.id AND price > 0 ORDER BY price ASC LIMIT 1) as price'),
                DB::raw('(SELECT amount FROM shop_pricematrixes WHERE id_product = '. $this->table . '.id AND price > 0 ORDER BY amount ASC LIMIT 1) as minimum_amount')
            ])
            ->where($this->table .'.deleted', '=', false)
            ->get();
    }


}
