@extends('website-cms::base')

@section('content')
    <div class="meta">
        {{csrf_field()}}
        <input type="hidden" name="model" value="{{$scope['table']}}">

    </div>
    <div class="containerList">
        <div class="row">
            <div class="col-md-12">
                <br/>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ $scope['icon'] }}"/> {{ $scope['title'] }}
                            </div>
                            <div class="col-md-10">
                                Wijzigen
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="post" action="/cms/{{$scope['table']}}/store" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$record->id}}">

                            @foreach($record->content as $key => $field)
                                @foreach($field as $fieldName => $content)
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="hidden" name="{{$key}}[{{$fieldName}}][type]" value="{{$content['type']}}">
                                            @if($content['type'] === 'text')
                                                <div class="form-group col-8">
                                                    <label for="name"><b>{{$key}}</b> [{{$fieldName}}]</label>
                                                    <input type="text" class="form-control"
                                                           name="{{$key}}[{{$fieldName}}][content]"
                                                           value="{{$content['content']}}">
                                                </div>

                                            @else
                                                <div class="form-group col-10">
                                                    <label for="template"><b>{{$key}}</b> [{{$fieldName}}]</label>
                                                    <textarea name="{{$key}}[{{$fieldName}}][content]"
                                                              class="form-control">{{$content['content']}}</textarea>
                                                </div>

                                            @endif
                                        </div>
                                    </div>

                                @endforeach
                                <hr/>
                        @endforeach

                    </div>

                </div>
                <hr/>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group col-3">
                            <button type="submit" class="btn btn-success">Opslaan</button>
                        </div>
                    </div>
                    <div class="col-6"></div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('js/select-images.js')}}" rel="stylesheet"></script>
@endsection

