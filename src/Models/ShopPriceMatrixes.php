<?php


namespace Gula\WebsiteCms\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class ShopPriceMatrixes
 * @package Gula\WebsiteCms\Models
 */
class ShopPriceMatrixes extends Model
{
    /**
     * @var string
     */
    protected $table = 'shop_pricematrixes';
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->table;
    }

    /**
     * @param int $idProduct
     * @return \Illuminate\Support\Collection
     */
    public function getPriceMatrix(int $idProduct)
    {
        return DB::table($this->table)
            ->where('id_product', '=', $idProduct)
            ->where('deleted', '=', false)
            ->orderBy('amount', 'asc')
            ->get();
    }

    /**
     * @param int $idProduct
     * @return array
     */
    public function getPriceSet(int $idProduct): array
    {
        $matrix = [];

        for ($i = 1; $i < 11; $i++) {
            $matrix[$i] = 0;
        }
        $priceSet = $this->getPriceMatrix($idProduct);

        foreach ($priceSet as $price) {
            if ($price->price > 0) {

                $matrix[$price->amount] = $price->price;
            } elseif(array_key_exists($price->amount - 1, $matrix) && array_key_exists($price->amount - 2, $matrix)) {
                $matrix[$price->amount] = $matrix[$price->amount - 1] + ($matrix[$price->amount - 1] - $matrix[$price->amount - 2]);
            } else {
                $matrix[$price->amount] = 0;
            }
        }
        return $matrix;
    }

    public function getProductPrice(int $idProduct, int $amount): float
    {
        $priceMatix = $this->getPriceMatrix($idProduct);

        $productPrice = 999;
        foreach ($priceMatix as $price){
            if($price->amount === $amount){
                $productPrice = $price->price;
            }
        }
        //@todo addiditional products

        return (float) $productPrice;
    }

    /**
     * @param int $idProduct
     * @return int
     */
    public function getMinimumAmount(int $idProduct): int
    {
        $result = DB::table($this->table)
            ->select(['amount'])
            ->where('id_product', '=', $idProduct)
            ->where('price', '>', 0)
            ->limit(1)
            ->orderBy('amount', 'asc')
            ->first();

        return $result->amount;
    }

}
