<?php


namespace Gula\WebsiteCms\Models;


use Illuminate\Support\Facades\DB;

class ShopCustomers
{
    protected $table = 'users';
    protected $tableRoles = 'roles';
    protected $tableRoleUser = 'role_user';

    public function getOne(int $id){
        return DB::table($this->table)->where(['id' => $id])->first();
    }

    public function getTableName(){
        return $this->table;
    }

}
